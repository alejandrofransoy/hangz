import { NgModule } from '@angular/core';
import { ErrorMessagesComponent } from './error-messages/error-messages';
import { CommonModule } from '@angular/common';
import { SkeletonCardComponent } from './skeleton-card/skeleton-card';
import { IonicModule } from 'ionic-angular'; //'ion-spinner' is not a known element

@NgModule({
	declarations: [ ErrorMessagesComponent,
    SkeletonCardComponent ],
	imports: [ IonicModule, CommonModule ],
	exports: [ ErrorMessagesComponent,
    SkeletonCardComponent ]
})
export class ComponentsModule {}
