import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { NoticesPage } from './notices';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    NoticesPage
  ],
  imports: [
    ComponentsModule,
    IonicPageModule.forChild(NoticesPage),
  ],
})
export class NoticesPageModule { }
