var express = require('express');
var https = require('https');
var bodyParser = require('body-parser');
var logger = require('morgan');
var cors = require('cors');
var SuperLogin = require('superlogin');

var app = express();
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cors());

var config = {
    dbServer: {
        protocol: 'https://',
        // host: '127.0.0.1:5984',
        // user: 'afransoy',
        // password: 'itmobile',
        host: '35.195.165.144:6984',
        user: 'admin',
        password: 'itmobile',
        cloudant: true,
        userDB: 'hangz-users',
        couchAuthDB: '_users'
    },
    security: {
        maxFailedLogins: 5,
        lockoutTime: 600,
        tokenLife: 604800, // one week
        loginOnRegistration: true
    },
    userDBs: {
        defaultDBs: {
            shared: ['hangz']
        },
        model: {
            hangz: {
                permissions: ['_reader', '_writer', '_replicator']
            }
        }
    },
    providers: {
        local: true
    }
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0" //OJO CON ESTO!! ES SOLO PARA QUE NO FALLE POR EL SELF SIGNED CERTIFICATE DEL COUCHDB EN BUTNAMI::QUITAR EN PROD!!!

// Initialize SuperLogin 
var superlogin = new SuperLogin(config);

// Mount SuperLogin's routes to our app 
app.use('/auth', superlogin.router);

app.listen(process.env.PORT || 8080);